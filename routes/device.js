module.exports = function(app, mongoose) {

    var deviceSchema = mongoose.Schema({
        name: String,
        power: String,
        room: String,
        imgUrl: String,
        typeOfDevice: String,
        dim: String,
        currentTemp: String,
        targetTemp: String
    });

    var Device = mongoose.model('Device', deviceSchema);

    app.get('/devices', function(req, res) {
        Device.find(function (err, devices) {
            if (err) {
                return console.error(err);
            }
            res.send(devices);
        });
    });

    app.get('/rooms', function(req, res) {
        Device.find().distinct('room', function(err, rooms) {
            if (err) {
                console.error(err);
            } else {
                res.send(rooms);
            }
        });
    });

    app.post('/device', function(req, res) {

        var device = new Device({
                name: req.body.name,
                power: req.body.power,
                room: req.body.room,
                imgUrl: req.body.imgUrl,
                typeOfDevice: req.body.typeOfDevice,
                dim: req.body.dim,
                currentTemp: req.body.currentTemp,
                targetTemp: req.body.targetTemp
            });

        device.save(function(err, device){
            if(err){
                console.error(err);
            }
        });

        res.send({msg: req.body.name + ' is saved.'});
    });

    app.delete('/device/:id', function(req, res) {
        Device.findOneAndRemove({'_id': req.params.id}, function (err) {
            if (err) {
                return console.error(err);
            } else {
                return console.log("Removed document.");
            }
        });
    });

    app.post('/update-device', function(req, res) {
        Device.update({_id: req.body.id}, {
            name: req.body.name,
            power: req.body.power,
            room: req.body.room,
            imgUrl: req.body.imgUrl,
            typeOfDevice: req.body.typeOfDevice,
            dim: req.body.dim,
            currentTemp: req.body.currentTemp,
            targetTemp: req.body.targetTemp
        }, function (err, device) {
            if (err) {
                res.send({msg: 'Error with the update.'});
            } else {
                res.send({msg: req.body.dim + ' is updated.'});
            }
        });
    });

}
