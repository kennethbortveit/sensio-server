var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
var mongoose = require('mongoose');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io').listen(http);


// Mongoose setup
mongoose.connect('mongodb://localhost/sensio');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

// Routes
require('./routes/device')(app, mongoose);

// Socket.io
io.on('connection', function(socket){
    console.log('A user connected.');

    socket.on('disconnect', function() {
        console.log('A user disconnected.');
    });

    socket.on('update-devices', function(data) {
        io.emit('update', {msg: 'Updating devices.'});
    });

    socket.on('update-device', function(data) {
        var device = JSON.parse(data);
        io.emit('update-single-device', device);
    });

    socket.on('delete-device', function(data) {
        io.emit('update', {msg: 'A device has been deleted.'});
    });
});

http.listen(4000, function(){
    console.log('Socket server is listening to port: 4000.');
});



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
