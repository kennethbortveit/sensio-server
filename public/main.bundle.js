webpackJsonp([1,4],{

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__device__ = __webpack_require__(26);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Light; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var Light = (function (_super) {
    __extends(Light, _super);
    function Light(name, room, typeOfDevice) {
        var _this = _super.call(this, name, room, typeOfDevice) || this;
        _this.dim = 50;
        return _this;
    }
    Light.prototype.setDim = function (dim) {
        this.dim = dim;
    };
    Light.prototype.getDim = function () {
        return this.dim;
    };
    return Light;
}(__WEBPACK_IMPORTED_MODULE_0__device__["a" /* Device */]));

//# sourceMappingURL=light.js.map

/***/ }),

/***/ 144:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__device__ = __webpack_require__(26);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Temprature; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var Temprature = (function (_super) {
    __extends(Temprature, _super);
    function Temprature(name, room, typeOfDevice) {
        var _this = _super.call(this, name, room, typeOfDevice) || this;
        _this.targetTemp = 20;
        _this.currentTemp = 20;
        return _this;
    }
    Temprature.prototype.getTargetTemp = function () {
        return this.targetTemp;
    };
    Temprature.prototype.setTargetTemp = function (degrees) {
        this.targetTemp = degrees;
    };
    Temprature.prototype.getCurrentTemp = function () {
        return this.currentTemp;
    };
    Temprature.prototype.setCurrentTemp = function (degrees) {
        this.currentTemp = degrees;
    };
    return Temprature;
}(__WEBPACK_IMPORTED_MODULE_0__device__["a" /* Device */]));

//# sourceMappingURL=temprature.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Room; });
var Room = (function () {
    function Room(name) {
        this.name = name;
        this.devices = [];
    }
    Room.prototype.getName = function () {
        return this.name;
    };
    Room.prototype.getDevices = function () {
        return this.devices;
    };
    Room.prototype.addDevice = function (device) {
        this.devices.push(device);
    };
    return Room;
}());

//# sourceMappingURL=room.js.map

/***/ }),

/***/ 189:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 189;


/***/ }),

/***/ 190:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(214);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__(298),
        styles: [__webpack_require__(284)]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_material__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_hammerjs__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_hammerjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_device_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_component__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__navigation_navigation_component__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__home_home_component__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__control_control_component__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__device_device_component__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__device_list_device_list_component__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__virtual_home_virtual_home_component__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__vr_device_vr_device_component__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__vr_room_vr_room_component__ = __webpack_require__(213);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_10__navigation_navigation_component__["a" /* NavigationComponent */],
            __WEBPACK_IMPORTED_MODULE_11__home_home_component__["a" /* HomeComponent */],
            __WEBPACK_IMPORTED_MODULE_12__control_control_component__["a" /* ControlComponent */],
            __WEBPACK_IMPORTED_MODULE_13__device_device_component__["a" /* DeviceComponent */],
            __WEBPACK_IMPORTED_MODULE_14__device_list_device_list_component__["a" /* DeviceListComponent */],
            __WEBPACK_IMPORTED_MODULE_15__virtual_home_virtual_home_component__["a" /* VirtualHomeComponent */],
            __WEBPACK_IMPORTED_MODULE_16__vr_device_vr_device_component__["a" /* VrDeviceComponent */],
            __WEBPACK_IMPORTED_MODULE_17__vr_room_vr_room_component__["a" /* VrRoomComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* RouterModule */].forRoot([
                { path: '', redirectTo: '/home', pathMatch: 'full' },
                { path: 'home', component: __WEBPACK_IMPORTED_MODULE_11__home_home_component__["a" /* HomeComponent */] },
                { path: 'control', component: __WEBPACK_IMPORTED_MODULE_12__control_control_component__["a" /* ControlComponent */] },
                { path: 'virtual-home', component: __WEBPACK_IMPORTED_MODULE_15__virtual_home_virtual_home_component__["a" /* VirtualHomeComponent */] }
            ]),
            __WEBPACK_IMPORTED_MODULE_6__angular_material__["a" /* MaterialModule */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_8__services_device_service__["a" /* DeviceService */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_device_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__device_device__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__device_light__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__device_temprature__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_socket_io_client__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_socket_io_client__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ControlComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ControlComponent = (function () {
    function ControlComponent(deviceService) {
        this.socket = __WEBPACK_IMPORTED_MODULE_5_socket_io_client__('http://localhost:4000');
        this.deviceService = deviceService;
        this.inputDeviceTypeOf = "device";
    }
    ControlComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.socket.on('update', function (msg) {
            console.log(msg);
            _this.update();
        });
        this.socket.on('update-single-device', function (data) {
            _this.deviceList.updateSingleDevice(data);
        });
    };
    ControlComponent.prototype.addDevice = function () {
        if (this.inputDeviceTypeOf == 'light') {
            this.deviceService.addDevice(new __WEBPACK_IMPORTED_MODULE_3__device_light__["a" /* Light */](this.inputDeviceName, this.inputDeviceRoom, this.inputDeviceTypeOf));
        }
        else if (this.inputDeviceTypeOf == 'temprature') {
            this.deviceService.addDevice(new __WEBPACK_IMPORTED_MODULE_4__device_temprature__["a" /* Temprature */](this.inputDeviceName, this.inputDeviceRoom, this.inputDeviceTypeOf));
        }
        else {
            this.deviceService.addDevice(new __WEBPACK_IMPORTED_MODULE_2__device_device__["a" /* Device */](this.inputDeviceName, this.inputDeviceRoom, this.inputDeviceTypeOf));
        }
        this.resetDeviceInput();
        this.socket.emit('update-devices', { msg: 'Socket update.' });
    };
    ControlComponent.prototype.update = function () {
        this.deviceList.fetchDevices();
    };
    ControlComponent.prototype.resetDeviceInput = function () {
        this.inputDeviceName = '';
        this.inputDeviceRoom = '';
        this.inputDeviceTypeOf = "device";
    };
    return ControlComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_6" /* ViewChild */])('deviceList'),
    __metadata("design:type", Object)
], ControlComponent.prototype, "deviceList", void 0);
ControlComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Component */])({
        selector: 'app-control',
        template: __webpack_require__(299),
        styles: [__webpack_require__(285)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_device_service__["a" /* DeviceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_device_service__["a" /* DeviceService */]) === "function" && _a || Object])
], ControlComponent);

var _a;
//# sourceMappingURL=control.component.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_device_service__ = __webpack_require__(31);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DeviceListComponent = (function () {
    function DeviceListComponent(deviceService) {
        this.deviceService = deviceService;
    }
    DeviceListComponent.prototype.ngOnInit = function () {
        this.fetchDevices();
    };
    DeviceListComponent.prototype.fetchDevices = function () {
        var _this = this;
        this.deviceService.fetchDevices().subscribe(function (devices) {
            _this.devices = devices;
        });
    };
    DeviceListComponent.prototype.removeDevice = function (id) {
        this.deviceService.removeDevice(id);
        this.socket.emit('delete-device', { msg: 'A device has been deleted.' });
    };
    DeviceListComponent.prototype.updateDevice = function (device) {
        this.deviceService.updateDevice(device);
        this.socket.emit('update-device', JSON.stringify(device));
    };
    DeviceListComponent.prototype.updateSingleDevice = function (device) {
        var y = this.findDevice(device['id']);
        y.setName(device['name']);
        y.setRoom(device['room']);
        y.setTypeOfDevice(device['typeOfDevice']);
        y.setImgUrl(device['typeOfDevice']);
        y.setPower(device['power']);
        if (device['typeOfDevice'] == 'temprature') {
            var temprature = y;
            temprature.setCurrentTemp(device['currentTemp']);
            temprature.setTargetTemp(device['targetTemp']);
        }
        else if (device['typeOfDevice'] == 'light') {
            var light = y;
            light.setDim(device['dim']);
        }
        else {
            console.log('This is just a device.');
        }
    };
    DeviceListComponent.prototype.findDevice = function (id) {
        var x = null;
        for (var _i = 0, _a = this.devices; _i < _a.length; _i++) {
            var device = _a[_i];
            if (device.getId() == id) {
                x = device;
            }
        }
        return x;
    };
    return DeviceListComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Input */])(),
    __metadata("design:type", Object)
], DeviceListComponent.prototype, "socket", void 0);
DeviceListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Component */])({
        selector: 'app-device-list',
        template: __webpack_require__(300),
        styles: [__webpack_require__(286)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_device_service__["a" /* DeviceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_device_service__["a" /* DeviceService */]) === "function" && _a || Object])
], DeviceListComponent);

var _a;
//# sourceMappingURL=device-list.component.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__device_device__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_device_service__ = __webpack_require__(31);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DeviceComponent = (function () {
    function DeviceComponent(modalService, deviceService) {
        this.deviceService = deviceService;
        this.modalService = modalService;
        this.removeButtonClicked = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* EventEmitter */]();
        this.updateButtonClicked = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* EventEmitter */]();
    }
    DeviceComponent.prototype.ngOnInit = function () {
    };
    DeviceComponent.prototype.open = function (content) {
        var _this = this;
        this.modalService.open(content).result.then(function (result) {
            _this.closeResult = "closed with: " + result;
        }, function (reason) {
            _this.closeResult = "Dismissed " + _this.getDismissReason(reason);
        });
    };
    DeviceComponent.prototype.getDismissReason = function (reason) {
        if (reason === __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* ModalDismissReasons */].ESC) {
            return 'by pressing ESC';
        }
        else if (reason === __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["b" /* ModalDismissReasons */].BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        }
        else {
            return "with: " + reason;
        }
    };
    DeviceComponent.prototype.removeDevice = function (id) {
        this.removeButtonClicked.emit(id);
    };
    DeviceComponent.prototype.update = function (device) {
        var _this = this;
        this.deviceService.updateDevice(device).subscribe(function (msg) {
            _this.device.status = "Updated.";
        });
        this.socket.emit('update-device', JSON.stringify(this.device));
    };
    DeviceComponent.prototype.updateSingleDevice = function () {
        var _this = this;
        this.deviceService.updateDevice(this.device).subscribe(function (msg) {
            _this.device.status = "Updated.";
        });
        this.updateButtonClicked.emit(this.device);
    };
    return DeviceComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Input */])(),
    __metadata("design:type", Object)
], DeviceComponent.prototype, "socket", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Input */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__device_device__["a" /* Device */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__device_device__["a" /* Device */]) === "function" && _a || Object)
], DeviceComponent.prototype, "device", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Output */])(),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* EventEmitter */]) === "function" && _b || Object)
], DeviceComponent.prototype, "removeButtonClicked", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_0" /* Output */])(),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["F" /* EventEmitter */]) === "function" && _c || Object)
], DeviceComponent.prototype, "updateButtonClicked", void 0);
DeviceComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Component */])({
        selector: 'app-device',
        template: __webpack_require__(301),
        styles: [__webpack_require__(287)]
    }),
    __metadata("design:paramtypes", [typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__ng_bootstrap_ng_bootstrap__["c" /* NgbModal */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_3__services_device_service__["a" /* DeviceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_device_service__["a" /* DeviceService */]) === "function" && _e || Object])
], DeviceComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=device.component.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Component */])({
        selector: 'app-home',
        template: __webpack_require__(302),
        styles: [__webpack_require__(288)]
    }),
    __metadata("design:paramtypes", [])
], HomeComponent);

//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export NavigationItem */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ITEMS; });
var NavigationItem = (function () {
    function NavigationItem(title, link) {
        this.title = title;
        this.link = link;
    }
    NavigationItem.prototype.getTitle = function () {
        return this.title;
    };
    NavigationItem.prototype.getLink = function () {
        return this.link;
    };
    return NavigationItem;
}());

var ITEMS = [
    new NavigationItem('Hjem', 'home'),
    new NavigationItem('Kontroll', 'control'),
    new NavigationItem('Virtuelt hjem', 'virtual-home')
];
//# sourceMappingURL=navigation-item.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__navigation_item__ = __webpack_require__(209);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavigationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NavigationComponent = (function () {
    function NavigationComponent() {
        this.items = __WEBPACK_IMPORTED_MODULE_1__navigation_item__["a" /* ITEMS */];
    }
    NavigationComponent.prototype.ngOnInit = function () {
    };
    return NavigationComponent;
}());
NavigationComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Component */])({
        selector: 'app-navigation',
        template: __webpack_require__(303),
        styles: [__webpack_require__(289)]
    }),
    __metadata("design:paramtypes", [])
], NavigationComponent);

//# sourceMappingURL=navigation.component.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_device_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_socket_io_client__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_socket_io_client__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VirtualHomeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var VirtualHomeComponent = (function () {
    function VirtualHomeComponent(deviceService) {
        this.socket = __WEBPACK_IMPORTED_MODULE_2_socket_io_client__('http://localhost:4000');
        this.deviceService = deviceService;
        this.devices = [];
        this.rooms = [];
    }
    VirtualHomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.update();
        this.socket.on('update-single-device', function (data) {
            var device = _this.findDevice(data['id']);
            device.setPower(data['power']);
            if (data['typeOfDevice'] == 'light') {
                var light = device;
                light.setDim(data['dim']);
            }
            if (data['typeOfDevice'] == 'temprature') {
                var temprature = device;
                temprature.setCurrentTemp(data['currentTemp']);
                temprature.setTargetTemp(data['targetTemp']);
            }
        });
        this.socket.on('update', function (data) {
            _this.update();
        });
    };
    VirtualHomeComponent.prototype.findDevice = function (id) {
        var device = null;
        for (var _i = 0, _a = this.devices; _i < _a.length; _i++) {
            var x = _a[_i];
            if (x.getId() == id) {
                device = x;
            }
        }
        return device;
    };
    VirtualHomeComponent.prototype.update = function () {
        var _this = this;
        this.deviceService.fetchDevices().subscribe(function (devices) {
            _this.devices = devices;
            _this.deviceService.fetchRooms().subscribe(function (rooms) {
                _this.rooms = rooms;
                for (var _i = 0, _a = _this.devices; _i < _a.length; _i++) {
                    var device = _a[_i];
                    for (var _b = 0, _c = _this.rooms; _b < _c.length; _b++) {
                        var room = _c[_b];
                        if (device.getRoom() == room.getName()) {
                            room.addDevice(device);
                        }
                    }
                }
            });
        });
    };
    return VirtualHomeComponent;
}());
VirtualHomeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Component */])({
        selector: 'app-virtual-home',
        template: __webpack_require__(304),
        styles: [__webpack_require__(290)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_device_service__["a" /* DeviceService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_device_service__["a" /* DeviceService */]) === "function" && _a || Object])
], VirtualHomeComponent);

var _a;
//# sourceMappingURL=virtual-home.component.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__device_device__ = __webpack_require__(26);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VrDeviceComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var VrDeviceComponent = (function () {
    function VrDeviceComponent() {
    }
    VrDeviceComponent.prototype.ngOnInit = function () {
    };
    return VrDeviceComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Input */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__device_device__["a" /* Device */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__device_device__["a" /* Device */]) === "function" && _a || Object)
], VrDeviceComponent.prototype, "device", void 0);
VrDeviceComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Component */])({
        selector: 'app-vr-device',
        template: __webpack_require__(305),
        styles: [__webpack_require__(291)]
    }),
    __metadata("design:paramtypes", [])
], VrDeviceComponent);

var _a;
//# sourceMappingURL=vr-device.component.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vr_room_room__ = __webpack_require__(145);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VrRoomComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var VrRoomComponent = (function () {
    function VrRoomComponent() {
    }
    VrRoomComponent.prototype.ngOnInit = function () {
    };
    return VrRoomComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Input */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__vr_room_room__["a" /* Room */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__vr_room_room__["a" /* Room */]) === "function" && _a || Object)
], VrRoomComponent.prototype, "room", void 0);
VrRoomComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Component */])({
        selector: 'app-vr-room',
        template: __webpack_require__(306),
        styles: [__webpack_require__(292)]
    }),
    __metadata("design:paramtypes", [])
], VrRoomComponent);

var _a;
//# sourceMappingURL=vr-room.component.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 26:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Device; });
/* unused harmony export TESTDEVICES */
var Device = (function () {
    function Device(name, room, typeOfDevice) {
        this.name = name;
        this.power = 'off';
        this.room = room;
        this.setImgUrl(typeOfDevice);
        this.typeOfDevice = typeOfDevice;
        this.status = "";
    }
    Device.prototype.getName = function () {
        return this.name;
    };
    Device.prototype.setName = function (name) {
        this.name = name;
    };
    Device.prototype.getPower = function () {
        return this.power;
    };
    Device.prototype.setPower = function (power) {
        this.power = power;
    };
    Device.prototype.getRoom = function () {
        return this.room;
    };
    Device.prototype.setRoom = function (room) {
        this.room = room;
    };
    Device.prototype.getImgUrl = function () {
        return this.imgUrl;
    };
    Device.prototype.setImgUrl = function (typeOfDevice) {
        if (typeOfDevice == 'light') {
            this.imgUrl = '/assets/img/light-bulb.png';
        }
        else if (typeOfDevice == 'temprature') {
            this.imgUrl = '/assets/img/temprature.png';
        }
        else {
            this.imgUrl = '/assets/img/device-symbol.png';
        }
    };
    Device.prototype.setId = function (id) {
        this.id = id;
    };
    Device.prototype.getId = function () {
        return this.id;
    };
    Device.prototype.getTypeOfDevice = function () {
        return this.typeOfDevice;
    };
    Device.prototype.setTypeOfDevice = function (typeOfDevice) {
        this.typeOfDevice = typeOfDevice;
    };
    return Device;
}());

var TESTDEVICES = [
    new Device('Spotter', 'Stue', 'light'),
    new Device('Kaffetrakter', 'Kjøkken', ''),
    new Device('Taklampe', 'Soverom Kenneth', 'light'),
    new Device('Tempratur', 'Inngangsparti', 'temprature'),
    new Device('Tempratur', 'Soverom 2', 'temprature')
];
//# sourceMappingURL=device.js.map

/***/ }),

/***/ 284:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 285:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 286:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 287:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)();
// imports


// module
exports.push([module.i, ".stretch {\n    height: 100%;\n    width: 100%;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 288:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 289:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 290:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 291:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)();
// imports


// module
exports.push([module.i, ".card-img-top {\n    width: 100%;\n    height: 100%;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 292:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(9)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 298:
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-12\">\n            <app-navigation></app-navigation>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-12\">\n            <h1>Sensio forretningssak</h1>\n            <h2>Programvareutvikler</h2>\n        </div>\n    </div>\n    <div class=\"row\">\n        <div class=\"col-12\">\n            <router-outlet></router-outlet>\n        </div>\n    </div>\n</div>\n\n"

/***/ }),

/***/ 299:
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-12\">\n        <h3 class=\"mb-5\">Kontroll</h3>\n    </div>\n</div>\n<div class=\"row\">\n    <div class=\"col-6\">\n        <div class=\"card mb-5\">\n            <div class=\"card-block\">\n                <h4 class=\"card-title\">Legg til enhet</h4>\n                <div class=\"row form-group\">\n                    <label for=\"deviceName\" class=\"col-3\">Navn:</label>\n                    <div class=\"col-9\">\n                        <input type=\"text\" class=\"form-control\" name=\"deviceName\" [(ngModel)]=\"inputDeviceName\" />\n                    </div>\n                </div>\n\n                <div class=\"row form-group\">\n                    <label for=\"deviceRoom\" class=\"col-3\">Rom:</label>\n                    <div class=\"col-9\">\n                        <input type=\"text\" class=\"form-control\" name=\"deviceRoom\" [(ngModel)]=\"inputDeviceRoom\" />\n                    </div>\n                </div>\n\n                <div class=\"row form-group\">\n                    <label for=\"deviceType\" class=\"col-3\">Type:</label>\n                    <div class=\"col-9\">\n                        <select class=\"form-control\" name=\"deviceType\" [(ngModel)]=\"inputDeviceTypeOf\">\n                            <option value=\"light\">Lys</option>\n                            <option value=\"temprature\">Tempratur</option>\n                            <option value=\"device\">Annen</option>\n                        </select>\n                    </div>\n                </div>\n\n                <div class=\"row form-group\">\n                    <div class=\"col-6\">\n                        <button type=\"button\" class=\"btn btn-primary d-block mr-auto\" (click)=\"addDevice()\">Legg til enhet</button>\n                    </div>\n                    <div class=\"col-6\">\n                        <button type=\"button\" class=\"btn btn-primary d-block ml-auto\" (click)=\"update()\">Oppdater</button>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n    <div class=\"col-6\">\n\n    </div>\n</div>\n\n<div class=\"row\">\n    <div class=\"col-12\">\n        <app-device-list #deviceList [socket]=\"socket\"></app-device-list>\n    </div>\n</div>\n"

/***/ }),

/***/ 300:
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-12\">\n        <h2>Enheter</h2>\n    </div>\n</div>\n<div class=\"row\">\n    <div class=\"col-4\" *ngFor=\"let device of devices\">\n        <app-device [device]=\"device\" [socket]=\"socket\" (removeButtonClicked)=\"removeDevice($event)\" (updateButtonClicked)=\"updateDevice($event)\"></app-device>\n    </div>\n</div>\n"

/***/ }),

/***/ 301:
/***/ (function(module, exports) {

module.exports = "<ng-template #content let-c=\"close\" let-d=\"dismiss\">\n\n\n    <div class=\"modal-header\">\n        <h4 class=\"modal-title\">{{ device.getName() }}</h4>\n        <button type=\"button\" class=\"close\" aria-label=\"Close\" (click)=\"d('Cross click')\">\n            <span aria-hidden=\"true\">&times;</span>\n        </button>\n    </div>\n\n\n    <div class=\"modal-body\">\n        <p>Enhet ID: {{ device.getId() }}.</p>\n        <p>Rom: {{ device.getRoom() }}.</p>\n        <p>Status: {{ device.status }}</p>\n        <fieldset class=\"form-group\">\n            <legend>Strøm</legend>\n            <div class=\"form-check\">\n                <label class=\"form-check-label\">\n                    <input type=\"radio\" class=\"form-check-input\" name=\"powerRadios\" id=\"powerOn\" value=\"on\" [(ngModel)]=\"device.power\" (change)=\"updateSingleDevice()\">På</label>\n            </div>\n            <div class=\"form-check\">\n                <label class=\"form-check-label\">\n                    <input type=\"radio\" class=\"form-check-input\" name=\"powerRadios\" id=\"powerOff\" value=\"off\" [(ngModel)]=\"device.power\" (change)=\"updateSingleDevice()\">Av</label>\n            </div>\n        </fieldset>\n        <fieldset class=\"form-group\" *ngIf=\"device.getTypeOfDevice() == 'light'\">\n            <legend>Dimmer</legend>\n            <p>Dimmer er nå på: {{ device.getDim() }}</p>\n            <md-slider [(ngModel)]=\"device.dim\" (change)=\"updateSingleDevice()\"></md-slider>\n        </fieldset>\n        <fieldset class=\"form-group\" *ngIf=\"device.getTypeOfDevice() == 'temprature'\">\n            <legend>Temperatur</legend>\n            <p>Nåværende temperatur: {{ device.getCurrentTemp() }}C</p>\n            <p>Ønsket temperatur: {{ device.getTargetTemp() }}C</p>\n            <md-slider [(ngModel)]=\"device.targetTemp\" (change)=\"updateSingleDevice()\"></md-slider>\n        </fieldset>\n    </div>\n\n\n    <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"updateSingleDevice()\">Oppdater</button>\n        <button type=\"button\" class=\"btn btn-danger\" (click)=\"c('Close click')\">Lukk</button>\n    </div>\n\n\n</ng-template>\n\n\n\n\n\n<div class=\"card mb-4\">\n    <img src=\"{{ device.getImgUrl() }}\" class=\"img-fluid card-img-top stretch\" />\n    <div class=\"card-block\">\n        <div class=\"row\">\n            <div class=\"col-12\">\n                <h4 class=\"card-title\">{{ device.getName() }}</h4>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-12\">\n                <p>Rom: {{ device.getRoom() }}</p>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-6\">\n                <button class=\"btn btn-primary d-block mr-auto\" (click)=\"open(content)\">Kontroller</button>\n            </div>\n            <div class=\"col-6\">\n                <button class=\"btn btn-primary d-block ml-auto\" (click)=\"removeDevice(device.getId())\">Fjern</button>\n            </div>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ 302:
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-12\">\n        <h3>Hjem</h3>\n    </div>\n</div>\n<div class=\"row\">\n    <div class=\"col-12\">\n        <p>Prosjektet kan klones via git-linkene nedenfor.</p>\n        <p>Noe av applikasjonens funksjonalitet krever at cache i nettleser er skrudd av. Se hvordan man gjør det i Firefox under.</p>\n    </div>\n</div>\n<div class=\"row\">\n    <div class=\"col-12\">\n        <h4>Git</h4>\n        <p>Client git repo: https://bitbucket.org/kennethbortveit/sensio-client.git</p>\n        <p>Server git repo: https://bitbucket.org/kennethbortveit/sensio-server.git</p>\n\n        <h4>Skru av cache (Firefox)</h4>\n        <ol>\n            <li>Åpne nettleser.</li>\n            <li>Skriv \"about:config\" i søkefeltet.</li>\n            <li>Klikk på \"I accept the risk!\".</li>\n            <li>Søk opp \"browser.cache.disk.enable\".</li>\n            <li>Dobbel-klikk slik at verdien blir \"false\".</li>\n            <li>Lukk Firefox og start nettleseren på nytt.</li>\n            <li>Søk opp \"browser.cache.memory.enable\".</li>\n            <li>Dobbel-klikk slik at verdien blir \"false\".</li>\n            <li>Lukk Firefox og start nettleseren på nytt.</li>\n        </ol>\n    </div>\n</div>\n"

/***/ }),

/***/ 303:
/***/ (function(module, exports) {

module.exports = "<ul class=\"nav nav-pills nav-fill\">\n    <li *ngFor=\"let item of items\" class=\"nav-item\">\n        <a routerLink=\"{{ item.getLink() }}\" class=\"nav-link\" routerLinkActive=\"active\">{{ item.getTitle() }}</a>\n    <li>\n</ul>\n"

/***/ }),

/***/ 304:
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-12\">\n        <h3 class=\"mb-5\">Virtuelt hjem</h3>\n    </div>\n</div>\n<div class=\"row\">\n    <div class=\"col-12\">\n        <app-vr-room *ngFor=\"let room of rooms\" [room]=\"room\"></app-vr-room>\n    </div>\n</div>\n"

/***/ }),

/***/ 305:
/***/ (function(module, exports) {

module.exports = "<div class=\"card mb-4\">\n    <div class=\"row\">\n        <div class=\"col-12\">\n            <img src=\"{{ device.getImgUrl() }}\" class=\"img-fluid d-block mx-auto\" />\n        </div>\n    </div>\n    <div class=\"card-block\">\n        <h4 class=\"card-title\">{{ device.getName() }}</h4>\n    </div>\n    <ul class=\"list-group list-group-flush\">\n        <li class=\"list-group-item\">Id: {{ device.getId() }}</li>\n        <li class=\"list-group-item\">Navn: {{ device.getName() }}</li>\n        <li class=\"list-group-item\">Type: {{ device.getTypeOfDevice() }}</li>\n        <li class=\"list-group-item\">Strøm: {{ device.getPower() }}</li>\n        <li class=\"list-group-item\" *ngIf=\"device.getTypeOfDevice() == 'light'\">Dimmer: {{ device.getDim() }}</li>\n        <li class=\"list-group-item\" *ngIf=\"device.getTypeOfDevice() == 'temprature'\">Temperatur: {{ device.getCurrentTemp() }}</li>\n        <li class=\"list-group-item\" *ngIf=\"device.getTypeOfDevice() == 'temprature'\">Ønsket tempratur: {{ device.getTargetTemp() }}</li>\n        \n    </ul>\n</div>\n"

/***/ }),

/***/ 306:
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-12\">\n        <ngb-accordion #acc=\"ngbAccordion\">\n            <ngb-panel title=\"{{ room.getName() }}\">\n                <ng-template ngbPanelContent>\n                    <div class=\"row\">\n                        <div class=\"col-4\" *ngFor=\"let device of room.getDevices()\">\n                            <app-vr-device [device]=\"device\"></app-vr-device>\n                        </div>\n                    </div>\n                </ng-template>\n            </ngb-panel>\n        </ngb-accordion>\n    </div>\n</div>\n"

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__device_device__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__device_light__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__device_temprature__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__vr_room_room__ = __webpack_require__(145);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeviceService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var DeviceService = (function () {
    function DeviceService(http) {
        this.http = http;
        this.deviceUrl = 'http://localhost:3000/device';
        this.devicesUrl = 'http://localhost:3000/devices';
        this.updateDeviceUrl = 'http://localhost:3000/update-device';
        this.roomsUrl = 'http://localhost:3000/rooms';
    }
    DeviceService.prototype.fetchDevices = function () {
        return this.http.get(this.devicesUrl).map(function (response) {
            var deviceJson = response.json();
            var deviceObjects = [];
            for (var _i = 0, deviceJson_1 = deviceJson; _i < deviceJson_1.length; _i++) {
                var device = deviceJson_1[_i];
                if (device.typeOfDevice == 'light') {
                    var light = new __WEBPACK_IMPORTED_MODULE_3__device_light__["a" /* Light */](device.name, device.room, device.typeOfDevice);
                    light.setId(device._id);
                    light.setDim(device.dim);
                    light.setPower(device.power);
                    deviceObjects.push(light);
                }
                else if (device.typeOfDevice == 'temprature') {
                    var temprature = new __WEBPACK_IMPORTED_MODULE_4__device_temprature__["a" /* Temprature */](device.name, device.room, device.typeOfDevice);
                    temprature.setId(device._id);
                    temprature.setCurrentTemp(device.currentTemp);
                    temprature.setTargetTemp(device.targetTemp);
                    temprature.setPower(device.power);
                    deviceObjects.push(temprature);
                }
                else {
                    var other = new __WEBPACK_IMPORTED_MODULE_2__device_device__["a" /* Device */](device.name, device.room, device.typeOfDevice);
                    other.setId(device._id);
                    other.setPower(device.power);
                    deviceObjects.push(other);
                }
            }
            return deviceObjects;
        });
    };
    DeviceService.prototype.addDevice = function (device) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers });
        this.http.post(this.deviceUrl, JSON.stringify(device), options).map(function (response) {
            return response.json();
        }).subscribe(function (msg) {
            console.log(msg);
        });
    };
    DeviceService.prototype.removeDevice = function (id) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'json/application' });
        this.http.delete(this.deviceUrl + "/" + id, { headers: headers })
            .subscribe(function (res) {
            console.log(res);
        });
    };
    DeviceService.prototype.updateDevice = function (device) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* RequestOptions */]({ headers: headers });
        return this.http.post(this.updateDeviceUrl, JSON.stringify(device), options).map(function (response) {
            return JSON.stringify(response.json());
        });
    };
    DeviceService.prototype.fetchRooms = function () {
        return this.http.get(this.roomsUrl).map(function (response) {
            var roomsJson = response.json();
            var roomsObjects = [];
            for (var _i = 0, roomsJson_1 = roomsJson; _i < roomsJson_1.length; _i++) {
                var room = roomsJson_1[_i];
                roomsObjects.push(new __WEBPACK_IMPORTED_MODULE_5__vr_room_room__["a" /* Room */](room));
            }
            return roomsObjects;
        });
    };
    return DeviceService;
}());
DeviceService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object])
], DeviceService);

var _a;
//# sourceMappingURL=device.service.js.map

/***/ }),

/***/ 363:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 365:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(190);


/***/ })

},[365]);
//# sourceMappingURL=main.bundle.js.map